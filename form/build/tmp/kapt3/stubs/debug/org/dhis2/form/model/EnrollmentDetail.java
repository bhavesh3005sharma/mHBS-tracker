package org.dhis2.form.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007\u00a8\u0006\b"}, d2 = {"Lorg/dhis2/form/model/EnrollmentDetail;", "", "(Ljava/lang/String;I)V", "ENROLLMENT_DATE_UID", "INCIDENT_DATE_UID", "ORG_UNIT_UID", "TEI_COORDINATES_UID", "ENROLLMENT_COORDINATES_UID", "form_debug"})
public enum EnrollmentDetail {
    /*public static final*/ ENROLLMENT_DATE_UID /* = new ENROLLMENT_DATE_UID() */,
    /*public static final*/ INCIDENT_DATE_UID /* = new INCIDENT_DATE_UID() */,
    /*public static final*/ ORG_UNIT_UID /* = new ORG_UNIT_UID() */,
    /*public static final*/ TEI_COORDINATES_UID /* = new TEI_COORDINATES_UID() */,
    /*public static final*/ ENROLLMENT_COORDINATES_UID /* = new ENROLLMENT_COORDINATES_UID() */;
    
    EnrollmentDetail() {
    }
}