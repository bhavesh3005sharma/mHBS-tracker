package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b\u00a8\u0006\f"}, d2 = {"Ldhis2/org/analytics/charts/data/ChartType;", "", "iconResource", "", "(Ljava/lang/String;II)V", "getIconResource", "()I", "LINE_CHART", "BAR_CHART", "TABLE", "SINGLE_VALUE", "NUTRITION", "dhis_android_analytics_dhisDebug"})
public enum ChartType {
    /*public static final*/ LINE_CHART /* = new LINE_CHART(0) */,
    /*public static final*/ BAR_CHART /* = new BAR_CHART(0) */,
    /*public static final*/ TABLE /* = new TABLE(0) */,
    /*public static final*/ SINGLE_VALUE /* = new SINGLE_VALUE(0) */,
    /*public static final*/ NUTRITION /* = new NUTRITION(0) */;
    private final int iconResource = 0;
    
    public final int getIconResource() {
        return 0;
    }
    
    ChartType(@androidx.annotation.DrawableRes()
    int iconResource) {
    }
}