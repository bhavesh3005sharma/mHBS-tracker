package dhis2.org.analytics.charts.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\u000b\u001a\u00020\u0003H\u00c2\u0003J\'\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0010\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0014\u001a\u00020\u000e2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Ldhis2/org/analytics/charts/data/NutritionGenderData;", "", "attributeUid", "", "femaleValue", "maleValue", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAttributeUid", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "isFemale", "value", "isMale", "toString", "dhis_android_analytics_dhisDebug"})
public final class NutritionGenderData {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String attributeUid = null;
    private final java.lang.String femaleValue = null;
    private final java.lang.String maleValue = null;
    
    public final boolean isFemale(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
        return false;
    }
    
    public final boolean isMale(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAttributeUid() {
        return null;
    }
    
    public NutritionGenderData(@org.jetbrains.annotations.NotNull()
    java.lang.String attributeUid, @org.jetbrains.annotations.NotNull()
    java.lang.String femaleValue, @org.jetbrains.annotations.NotNull()
    java.lang.String maleValue) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    private final java.lang.String component2() {
        return null;
    }
    
    private final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dhis2.org.analytics.charts.data.NutritionGenderData copy(@org.jetbrains.annotations.NotNull()
    java.lang.String attributeUid, @org.jetbrains.annotations.NotNull()
    java.lang.String femaleValue, @org.jetbrains.annotations.NotNull()
    java.lang.String maleValue) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}